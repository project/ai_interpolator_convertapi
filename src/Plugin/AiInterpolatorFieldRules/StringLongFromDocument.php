<?php

namespace Drupal\ai_interpolator_convertapi\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_convertapi\TextDocument;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * The rules for a string long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_convertapi_string_long_from_document",
 *   title = @Translation("String Long From Document"),
 *   field_rule = "string_long"
 * )
 */
class StringLongFromDocument extends TextDocument implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Text From Document';

}
