<?php

namespace Drupal\ai_interpolator_convertapi\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_convertapi\TextDocument;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * The rules for a text long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_convertapi_text_long_from_document",
 *   title = @Translation("Text Long From Document"),
 *   field_rule = "text_long"
 * )
 */
class TextLongFromDocument extends TextDocument implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Text From Document';

}
